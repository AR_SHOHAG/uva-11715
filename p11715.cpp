#include <cstdio>
#include <cmath>
#include <iostream>
using namespace std;

int main()
{
    double u, v, t, a, s;
    int n, ck=0;

    while(scanf("%d", &n),n>0){
        if(n==1){
            scanf("%lf%lf%lf", &u, &v, &t);
            a=(v-u)/t;
            s=(v*v-u*u)/(2*a);
            ///cout<<s<<endl;
            printf("Case %d: %.3lf %.3lf\n", ++ck, s, a);
        }
        else if(n==2){
            scanf("%lf%lf%lf", &u, &v, &a);
            s=(v*v-u*u)/(2*a);
            t=(v-u)/a;
            printf("Case %d: %.3lf %.3lf\n", ++ck, s, t);
        }
        else if(n==3){
            scanf("%lf%lf%lf", &u, &a, &s);
            v=sqrt((u*u+2*a*s));
            t=(v-u)/a;
            printf("Case %d: %.3lf %.3lf\n", ++ck,v, t);
        }
        else if(n==4){
            scanf("%lf%lf%lf", &v, &a, &s);
            u=sqrt((v*v-2*a*s));
            t=(v-u)/a;
            printf("Case %d: %.3lf %.3lf\n", ++ck, u, t);
        }
    }
    return 0;
}
